import { HttpUrlEncodingCodec } from '@angular/common/http';

export class CustomHttpUrlEncodingCodec extends HttpUrlEncodingCodec {

  encodeValue(k: string): string {
    return encodeURIComponent(k);
  }

}
