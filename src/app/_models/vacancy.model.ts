import { JobAreaModel } from './job-area.model';
import { LocationModel } from './location.model';
import { removeStyleFromHtml } from '../_helpers/remove-style-property.helper';

export class Vacancy {

  id: number;
  name: string;
  aboutTheProject: string;
  positionRequirements: string;
  aboutPosition: string;
  responsibilities: string;
  ourOtherClientsWhichWorkWith: string;
  wouldBePlus: string;
  weOffer: string;
  showAboutTheProject: boolean;
  showResponsibilities: boolean;
  showAboutPosition: boolean;
  showPositionRequirements: boolean;
  showOurOtherClientsWhichWorkWith: boolean;
  showWeOffer: boolean;
  showWouldBePlus: boolean;
  isRemote: boolean;
  isHotStatus: boolean;
  isShown: boolean;
  locations: LocationModel[];
  jobArea: JobAreaModel;

  constructor(
    id?: number,
    name?: string,
    aboutPosition?: string,
    showAboutPosition?: boolean,
    aboutTheProject?: string,
    showAboutTheProject?: boolean,
    positionRequirements?: string,
    showPositionRequirements?: boolean,
    ourOtherClientsWhichWorkWith?: string,
    showOurOtherClientsWhichWorkWith?: boolean,
    wouldBePlus?: string,
    showWouldBePlus?: boolean,
    weOffer?: string,
    showWeOffer?: boolean,
    responsibilities?: string,
    showResponsibilities?: boolean,
    isRemote?: boolean,
    isHotStatus?: boolean,
    isShown?: boolean,
    jobArea?: JobAreaModel,
    locations?: any[],
    public order?: number
  ) {
    this.id = id;
    this.name = name;
    this.locations = locations;
    this.jobArea = jobArea;
    this.aboutTheProject = removeStyleFromHtml(aboutTheProject);
    this.showAboutTheProject = showAboutTheProject;
    this.ourOtherClientsWhichWorkWith = removeStyleFromHtml(ourOtherClientsWhichWorkWith);
    this.showOurOtherClientsWhichWorkWith = showOurOtherClientsWhichWorkWith;
    this.weOffer = removeStyleFromHtml(weOffer);
    this.showWeOffer = showWeOffer;
    this.positionRequirements = removeStyleFromHtml(positionRequirements);
    this.showPositionRequirements = showPositionRequirements;
    this.wouldBePlus = removeStyleFromHtml(wouldBePlus);
    this.showWouldBePlus = showWouldBePlus;
    this.aboutPosition = removeStyleFromHtml(aboutPosition);
    this.showAboutPosition = showAboutPosition;
    this.responsibilities = removeStyleFromHtml(responsibilities);
    this.showResponsibilities = showResponsibilities;
    this.isRemote = isRemote;
    this.isHotStatus = isHotStatus;
    this.isShown = isShown;
  }

}
