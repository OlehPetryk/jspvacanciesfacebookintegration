import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { Vacancy } from '../_models/vacancy.model';
import { JobAreaModel } from '../_models/job-area.model';
import { LocationModel } from '../_models/location.model';
import { Page } from '../_models/page.model';
import { VacancyService } from '../_services/vacancy.service';
import { JobAreaService } from '../_services/job-area.service';
import { LocationService } from '../_services/location.service';
import { FilterType } from '../_enums/filter-type';

@Component({
  selector: 'jsp-vacancy-list',
  templateUrl: './vacancy-list.component.html',
  styleUrls: ['./vacancy-list.component.scss']
})
export class VacancyListComponent implements OnInit, OnDestroy {
  destroySubject: Subject<void> = new Subject();

  isCollapsed = false;
  collapseAllowsWidth = 768;
  filterType = FilterType;
  careers: Vacancy[] = [];
  locationPage: Page<LocationModel>;
  jobAreaPage: Page<JobAreaModel>;
  locations: LocationModel[];
  jobAreas: JobAreaModel[];
  selectedLocations: LocationModel[] = [];
  selectedJobAreas: JobAreaModel[] = [];
  keyWords: string[] = [];
  userFilter = [];
  filterPage: Page<Vacancy>;

  constructor(
    private vacancyService: VacancyService,
    private locationService: LocationService,
    private jobAreaService: JobAreaService
  ) {
    this.getLocations(1, 100);
    this.getJobAreas(1, 100);
  }

  getLocations(page: number, count?: number) {
    this.locationService.getLocations(page - 1, count).pipe(takeUntil(this.destroySubject))
      .subscribe(data => {
        this.locationPage = data;
        this.locations = this.locationPage.items;
      });
  }

  getJobAreas(page: number, count?: number) {
    this.jobAreaService.getJobAreas(page - 1, count).pipe(takeUntil(this.destroySubject))
      .subscribe(data => {
        this.jobAreaPage = data;
        this.jobAreas = this.jobAreaPage.items;
      });
  }

  getFilters(page: number, count?: number, filters?: any, paramsType?: FilterType) {
    const filtersUrl = [];
    if (paramsType === FilterType.KeyWords) {
      filtersUrl.push(filters);
    } else {
      filters.forEach(filter => {
        filtersUrl.push(filter.id);
      });
    }


    this.vacancyService.filterVacancy(page - 1, count, filtersUrl.join(), paramsType).pipe(takeUntil(this.destroySubject))
      .subscribe(data => {
          this.filterPage = data;
          this.careers = this.filterPage.items;
        }
      );
  }

  showCareers() {
    this.vacancyService.getAllVacancies(0, 100).pipe(takeUntil(this.destroySubject))
      .subscribe(
        careers => {
          this.careers = careers.items;
        }
      );
  }

  removeOldTagsGroup(filterType: FilterType) {
    switch (filterType) {

      case (FilterType.JobAreas):
        this.selectedLocations = [];
        this.keyWords = null;
        break;
      case (FilterType.Locations):

        this.selectedJobAreas = [];
        this.keyWords = null;
        break;

      case (FilterType.KeyWords):
        this.selectedLocations = [];
        this.selectedJobAreas = [];
        break;

    }
  }

  update(filterType, page?: number, count?: number, selectedFilter?: any) {
    switch (filterType) {

      case (FilterType.JobAreas):
        this.removeOldTagsGroup(filterType);
        this.userFilter = this.selectedJobAreas;
        this.getFilters(page || 1, count || 100, selectedFilter || this.selectedJobAreas, FilterType.JobAreas);
        break;

      case (FilterType.Locations):
        this.removeOldTagsGroup(filterType);
        const mappedLocations = this.selectedLocations.map(locations => {
          return {
            id: locations.id,
            name: locations.city + ', ' + locations.country
          };
        });
        this.userFilter = mappedLocations;
        this.getFilters(page || 1, count || 100, selectedFilter || mappedLocations, FilterType.Locations);
        break;

      case (FilterType.KeyWords):
        this.userFilter = [{ name: this.keyWords }];
        this.removeOldTagsGroup(filterType);
        this.getFilters(page || 1, count || 100, selectedFilter || this.keyWords, FilterType.KeyWords);
        if (!this.keyWords) {
          this.userFilter = [];
        }
        break;

    }
  }

  clear(obj) {
    if (this.selectedJobAreas.length) {
      this.selectedJobAreas = this.selectedJobAreas.filter(jobArea => jobArea !== obj);
      this.getFilters(1, 100, this.selectedJobAreas, FilterType.JobAreas);
    } else if (this.selectedLocations.length) {
      this.selectedLocations = this.selectedLocations.filter(location => location.id !== obj.id);
      this.getFilters(1, 100, this.selectedLocations, FilterType.Locations);
    } else if (this.keyWords.length) {
      this.keyWords = [];
      this.getFilters(1, 100, this.keyWords, FilterType.KeyWords);
    }
    this.userFilter = this.userFilter.filter(filter => filter !== obj);
  }

  windowWidth() {
    if (window.innerWidth < this.collapseAllowsWidth) {
      this.isCollapsed = !this.isCollapsed;
    }
  }

  ngOnInit() {
    this.showCareers();
  }

  ngOnDestroy() {
    this.destroySubject.next();
  }
}
