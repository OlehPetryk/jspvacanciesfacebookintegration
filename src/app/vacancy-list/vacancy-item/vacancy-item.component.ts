import { Component, Input } from '@angular/core';

import { Vacancy } from '../../_models/vacancy.model';

@Component({
  selector: 'jsp-vacancy-item',
  templateUrl: './vacancy-item.component.html',
  styleUrls: ['./vacancy-item.component.scss']
})
export class VacancyItemComponent  {
  @Input() vacancy: Vacancy;
}
