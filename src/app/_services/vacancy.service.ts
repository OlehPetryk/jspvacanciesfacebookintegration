import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Vacancy } from '../_models/vacancy.model';
import { ApiService } from './api.service';
import { Page } from '../_models/page.model';
import { FilterType } from '../_enums/filter-type';
import { CustomHttpUrlEncodingCodec } from '../_guards/custom-http-encode';

@Injectable()

export class VacancyService {

  constructor(private apiService: ApiService) {}

  public getAllVacancies(page: number, count?: number): Observable<Page<Vacancy>> {
    return this.apiService.getPage<Page<Vacancy>>('api/v1/vacancies', page, count);
  }

  filterVacancy(
    page: number = 0,
    count?: number,
    filter?: string,
    paramsType?: FilterType
  ): Observable<Page<Vacancy>> {
    let params = new HttpParams({
      encoder: new CustomHttpUrlEncodingCodec()
    });
    params = params.append('count', `${ count }`);
    params = params.append('page', `${ page }`);
    if (filter.length) {
      switch (paramsType) {

        case (FilterType.JobAreas):
          params = params.append('jobAreas', `${ filter }`);
          break;

        case (FilterType.Locations):
          params = params.append('locations', `${ filter }`);
          break;

        case (FilterType.KeyWords):
          params = params.append('keyWords', `${ filter }`);
          break;

      }
    }
    return this.apiService.getWithParam<Page<Vacancy>>('api/v1/Vacancies/', params);
  }

}
