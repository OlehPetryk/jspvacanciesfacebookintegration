import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiService } from './api.service';
import { Page } from '../_models/page.model';
import { LocationModel } from '../_models/location.model';

@Injectable()
export class LocationService {

  constructor(private apiService: ApiService) {}

  public getLocations(page: number, count?: number): Observable<Page<LocationModel>> {
    return this.apiService.getPage<Page<LocationModel>>('api/v1/locations', page, count);
  }

}
