import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiService } from './api.service';
import { Page } from '../_models/page.model';
import { JobAreaModel } from '../_models/job-area.model';

@Injectable()
export class JobAreaService {

  constructor(private apiService: ApiService) {}

  public getJobAreas(page: number, count?: number): Observable<Page<JobAreaModel>> {
    return this.apiService.getPage<Page<JobAreaModel>>('api/v1/jobareas', page, count);
  }

}
