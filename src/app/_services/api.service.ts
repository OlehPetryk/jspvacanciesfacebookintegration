import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment } from '../../environments/environment';

@Injectable()
export class ApiService {

  readonly baseUrl: string;

  constructor(private http: HttpClient) {
    this.baseUrl = environment.baseApi;
  }

  public getWithParam<T>(actionURL: string, params: HttpParams): Observable<T> {
    return this.http.get<T>(this.baseUrl + actionURL, { params });
  }

  public getPage<T>(actionURL: string, page: number = 0, count?: number): Observable<T> {
    let params = new HttpParams();
    params = params.append('count', `${ count }`);
    params = params.append('page', `${ page }`);
    return this.http.get<T>(this.baseUrl + actionURL, { params });
  }

}
