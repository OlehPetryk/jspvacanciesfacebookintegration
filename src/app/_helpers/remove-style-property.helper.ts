export function removeStyleFromHtml(html: string): string {
    if (html) {
        const regexp = new RegExp(/ style=\"(.*?)\"/igm);
        const result = html.replace(regexp, '');
        return result;
    }
    return html;
}
