export enum FilterType {
    Services,
    Industries,
    Expertises,
    Technologies,
    JobAreas,
    Locations,
    KeyWords
}
