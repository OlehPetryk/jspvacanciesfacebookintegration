import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgSelectModule } from '@ng-select/ng-select';
import { HttpClientModule } from '@angular/common/http';
import { CollapseModule } from 'ngx-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { VacancyListComponent } from './vacancy-list/vacancy-list.component';
import { ApiService } from './_services/api.service';
import { JobAreaService } from './_services/job-area.service';
import { LocationService } from './_services/location.service';
import { VacancyService } from './_services/vacancy.service';
import { VacancyItemComponent } from './vacancy-list/vacancy-item/vacancy-item.component';
import { NoSanitizePipe } from './_pipes/no-sanitize.pipe';

@NgModule({
  declarations: [
    VacancyListComponent,
    VacancyItemComponent,
    NoSanitizePipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    CollapseModule.forRoot(),
    FormsModule,
    HttpClientModule,
    NgSelectModule
  ],
  providers: [
    ApiService,
    JobAreaService,
    LocationService,
    VacancyService
  ],
  bootstrap: [VacancyListComponent]
})
export class AppModule {}
